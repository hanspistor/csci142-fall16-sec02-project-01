package connectmodel;

import java.awt.Point;
import java.util.Arrays;
import java.util.Vector;


public class GameBoard
{

    /**
     *
     * GameBoard is the class that contains all the methods relating the board of the class such as placing a piece on the board,
     * or checking to see if there is a win
     *
     * @author hanspistor bryansingh
     * @version 1.0
     *
     * Date Due: Wednesday, October 19th, 2016
     *
     * Input: This program requires three integers : number of rows, number of columns, win length and one array of PieceTypes
     * Output: This program returns a GameBoard Object
     *
     */

    private int myNumRows;
    private int myNumColumns;
    private PieceType[][] myBoard;
    private int myNumTypes;
    private int myWinLength;
    private Point myLastPoint;
    private Vector<PieceType> myTypes;
    private Point myWinBegin;
    private Point myWinEnd;


    /**
     *
     * Constructs a GameBoard object given rows, columns, winlength, and an array of types
     *
     * @param rows
     * @param cols
     * @param winLength
     * @param types
     */
    public GameBoard(int rows, int cols, int winLength, PieceType[] types) 
    {
    	myNumRows = rows;
    	myNumColumns = cols;
    	myWinLength = winLength;
    	myBoard = new PieceType[cols][rows];
        fillWithNull();
    	myNumTypes = types.length;
    	myTypes = new Vector<PieceType>();
        for (PieceType type : types) {
            myTypes.add(type);
        }

    }

    /**
     *
     * Fills the entire board with null, effectively resetting the board
     *
     */

    private void fillWithNull() {
        for (int i = 0; i < myNumColumns; i++) {
            for (int j = 0; j < myNumRows; j++) {
                myBoard[i][j] = null;
            }
        }
    }


    /**
     *
     * Places a piece of PieceType type in the corresponding column
     *
     * @param col
     * @param type
     * @return <code>true</code> if the piece was allowed to be placed
     *         <code>false</code> if the piece wasn't allowed to be placed
     *
     */
    public boolean placePiece(int col, PieceType type)
    {
    	if (col < 0 || col >= myNumColumns) {
    		return false;
    	}
    	for (int i = 0; i < myNumRows; i++) {
    		if (myBoard[col][i] == null) {
    			myBoard[col][i] = type;
                myLastPoint = new Point(col, i);
    			return true;
    		}
    		
    	}
	    return false;
    }

    /**
     *
     * Resets the board, filling all the possible slots with null
     *
     */
    public void resetBoard() 
    {
    	fillWithNull();
    }

    /**
     *
     * Finds and returns the first empty row in column
     *
     * @param column
     * @return the first empty row in a column
     */
    private int findTopRow(int column) {
        for (int i = 0; i < myNumRows; i++) {
            if (getPieceOnBoard(new Point(column, i)) == null) {
                return i;
            }
        }
        return -1;
    }

    /**
     *
     * Finds the best column to place a piece of PieceType type in in the following order
     *  1). If it will result in a win for PieceType type
     *  2). If it will block a win for the other PieceType
     *  3). The one that will return the longest chain of pieces in a row
     *
     * @param type
     * @return best column to place piece in
     */
    public int findBestMoveColumn(PieceType type)
    {
        int max = -1;
        for (int i = 0; i < myNumColumns; i++) {
            if (countHorizontalLengthIfPiecePlaced(i, type) >= myWinLength || countVerticalLengthIfPiecePlaced(i, type) >= myWinLength
                    || countDiagonalLengthIfPiecePlaced(i, type) >= myWinLength) {
                System.out.println((countHorizontalLengthIfPiecePlaced(i, type) >= myWinLength) + " " + (countVerticalLengthIfPiecePlaced(i, type) >= myWinLength)
                        +" " + (countDiagonalLengthIfPiecePlaced(i, type) >= myWinLength));
                System.out.println("can win " + i);
                return i;
            }
            PieceType oppType = myTypes.get((myTypes.indexOf(type) + 1)%2);
            if (countHorizontalLengthIfPiecePlaced(i, oppType) >= myWinLength || countVerticalLengthIfPiecePlaced(i, oppType) >= myWinLength
                    || countDiagonalLengthIfPiecePlaced(i, oppType) >= myWinLength) {
                System.out.println("can lose" + i );
                return i;
            }
            max = Math.max(Math.max(max, countHorizontalLengthIfPiecePlaced(i, type)),
                    Math.max(countVerticalLengthIfPiecePlaced(i, type),countDiagonalLengthIfPiecePlaced(i, type)));
        }

        System.out.println(max);
        if (max == 0) {
            max = myNumColumns/2;
        }

        return max;
    }

    /**
     * Check all three possible directions for a win
     *  1). Vertically
     *  2). Horizontally
     *  3). Diagonally
     *
     * @return <code>true</code> if someone won
     *         <code>false</code> if no one won
     */
    public boolean checkIfWin() {
        return checkVerticalWin() || checkHorizontalWin() || checkDiagonalWin();
    }

    /**
     * Check for a vertical win
     * @return <code>true</code> if someone won
     *         <code>false</code> if no one won
     *
     */
    private boolean checkVerticalWin()
    {

        if (getLastPoint() == null) {
            return false;
        }
        if (getPieceOnBoard(getLastPoint()) == null) {
            return false;
        }

        Point last = getLastPoint();
        PieceType lastType = getPieceOnBoard(last);

        /*
            consecutivePieces[0] == up
            consecutivePieces[1] == down
         */

        int[] consecutivePieces = {0,0};
        for (int i = 0; i < myWinLength; i++) {
            if (last.y + i < myNumRows && getPieceOnBoard(new Point(last.x, last.y + i)) == lastType) {
                consecutivePieces[0]++;
            }
            if (last.y - i >= 0 && getPieceOnBoard(new Point(last.x, last.y - i)) == lastType) {
                consecutivePieces[1]++;
            }
        }

        if (consecutivePieces[0] == myWinLength) {
            myWinBegin = last;
            myWinEnd = new Point(last.x, last.y + (myWinLength - 1));
            return true;
        } else if (consecutivePieces[1] == myWinLength) {
            myWinBegin = last;
            myWinEnd = new Point(last.x, last.y - (myWinLength - 1));
            return true;
        }

	    return false;
    }

    /**
     * Check for a horizontal win
     * @return <code>true</code> if someone won
     *         <code>false</code> if no one won
     *
     */

    private boolean checkHorizontalWin()
    {

        if (getLastPoint() == null) {
            return false;
        }
        if (getPieceOnBoard(getLastPoint()) == null) {
            return false;
        }

        Point last = getLastPoint();
        PieceType lastType = getPieceOnBoard(last);
        /*
            consecutivePieces[0] == right
            consecutivePieces[1] == left
         */

        int[] consecutivePieces = {0,0};
        for (int i = 0; i < myWinLength; i++) {
            if (last.x + i < myNumColumns && getPieceOnBoard(new Point(last.x + i, last.y)) == lastType) {
                consecutivePieces[0]++;
            }
            if (last.x - i >= 0 && getPieceOnBoard(new Point(last.x - i, last.y)) == lastType) {
                consecutivePieces[1]++;
            }
        }

        if (consecutivePieces[0] == myWinLength) {
            myWinBegin = last;
            myWinEnd = new Point(last.x + (myWinLength - 1), last.y);
            return true;
        } else if (consecutivePieces[1] == myWinLength) {
            myWinBegin = last;
            myWinEnd = new Point(last.x - (myWinLength - 1), last.y);
            return true;
        }

	    return false;
    }

    /**
     * Check for a diagonal win
     * @return <code>true</code> if someone won
     *         <code>false</code> if no one won
     *
     */

    private boolean checkDiagonalWin() {

        if (getLastPoint() == null) {
            return false;
        }
        if (getPieceOnBoard(getLastPoint()) == null) {
            return false;
        }

        Point last = getLastPoint();
        PieceType lastType = getPieceOnBoard(last);
        /*
            consecutivePieces[0] = up and right
            consecutivePieces[1] = down and right
            consecutivePieces[2] = up left
            consecutivePieces[3] = down left
         */
        int[] consecutivePieces = {0,0,0,0};
        for (int i = 1; i < myWinLength; i++) {
            if (last.x + i < myNumColumns && last.y + i < myNumRows && getPieceOnBoard(new Point(last.x + i, last.y + i)) == lastType) {
                consecutivePieces[0]++;
            }
            if (last.x + i < myNumColumns && last.y - i >= 0 && getPieceOnBoard(new Point(last.x + i, last.y - i)) == lastType) {
                consecutivePieces[1]++;
            }
            if (last.x - i >= 0 && last.y + i < myNumRows && getPieceOnBoard(new Point(last.x - i, last.y + i)) == lastType) {
                consecutivePieces[2]++;
            }
            if (last.x - i >= 0 && last.y - i >= 0 && getPieceOnBoard(new Point(last.x - i, last.y - i)) == lastType) {
                consecutivePieces[3]++;
            }
        }
        int asscending = consecutivePieces[0] + consecutivePieces[3];
        int descending = consecutivePieces[1] + consecutivePieces[2];
        if (asscending >= myWinLength-1) {
            myWinBegin = new Point(last.x - consecutivePieces[3], last.y - consecutivePieces[3]);
            myWinEnd = new Point(myWinBegin.x + (myWinLength - 1), myWinBegin.y + (myWinLength - 1));
            return true;
        } else if (descending >= myWinLength-1) {
            myWinBegin = new Point(last.x - consecutivePieces[2], last.y + consecutivePieces[2]);
            myWinEnd = new Point(myWinBegin.x + (myWinLength - 1), myWinBegin.y - (myWinLength - 1));
            return true;
        }

        return false;


    }

    /**
     * if a piece of PieceType type was placed in Column col, how long would the horizontal chain of PieceType type be
     * @param col
     * @param type
     * @return the length of the chain of continuous horizontal pieces of PieceType type
     */
	
    private int countHorizontalLengthIfPiecePlaced(int col, PieceType type) 
    {
        int topRow = findTopRow(col);
        boolean left,right = true;
        int consec = 1; // 1 because the piece placed is already 1 in a row
        for (int i = 1; i < myWinLength; i++) {
            if (col + i < myNumColumns && myBoard[col+i][topRow] == type) {
                consec++;
            } else {
                right = false;
            }
            if (col - i >= 0 && myBoard[col-i][topRow] == type) {
                consec++;
            } else {
                left = false;
            }
        }
	    return consec;
    }

    /**
     * if a piece of PieceType type was placed in Column col, how long would the vertical chain of PieceType type be
     * @param col
     * @param type
     * @return the length of the chain of continuous vertical pieces of PieceType type
     */
	
    private int countVerticalLengthIfPiecePlaced(int col, PieceType type) 
    {
        int consec = 1; // 1 because the piece placed is already 1 in a row
        for (int i = findTopRow(col)-1; i >= 0; i++) {
            if (myBoard[col][i] == type) {
                consec++;
            } else {
                break;
            }
        }
	    return consec;
    }

    /**
     * if a piece of PieceType type was placed in Column col, how long would the diagonal chain of PieceType type be
     * @param col
     * @param type
     * @return the length of the chain of continuous diagonal pieces of PieceType type
     */

    private int countDiagonalLengthIfPiecePlaced(int col, PieceType type) 
    {
        Point last = new Point(col, findTopRow(col));
        /*
            consecutivePieces[0] = up and right
            consecutivePieces[1] = down and right
            consecutivePieces[2] = up left
            consecutivePieces[3] = down left
         */
        int[] consecutivePieces = {1,1,0,0}; // 1 in each direction is 0 in order to include the placed piece
        for (int i = 1; i < myWinLength; i++) {
            if (last.x + i < myNumColumns && last.y + i < myNumRows && getPieceOnBoard(new Point(last.x + i, last.y + i)) == type) {
                consecutivePieces[0]++;
            }
            if (last.x + i < myNumColumns && last.y - i >= 0 && getPieceOnBoard(new Point(last.x + i, last.y - i)) == type) {
                consecutivePieces[1]++;
            }
            if (last.x - i >= 0 && last.y + i < myNumRows && getPieceOnBoard(new Point(last.x - i, last.y + i)) == type) {
                consecutivePieces[2]++;
            }
            if (last.x - i >= 0 && last.y - i >= 0 && getPieceOnBoard(new Point(last.x - i, last.y - i)) == type) {
                consecutivePieces[3]++;
            }
        }
        int asscending = consecutivePieces[0] + consecutivePieces[3];
        int descending = consecutivePieces[1] + consecutivePieces[2];
        //returns max of the 4 directions
        System.out.println(consecutivePieces[1] + "    " +  consecutivePieces[2]);
        return Math.max(asscending, descending);
    }

    /**
     * return a vector of all PieceTypes in this GameBoard
     * @return Vector<PieceType> containing all PieceTypes on GameBoard
     */
    public Vector<PieceType> getTypes() 
    {
	    return myTypes;
    }

    /**
     *
     * @return Point where win begins
     *
     */
    public Point getWinBegin()
    {
	    return myWinBegin;
    }

    /**
     *
     * @return Point where win ends
     *
     */

    public Point getWinEnd()
    {
	    return myWinEnd;
    }

    /**
     *
     * @return last Point played
     *
     */

    public Point getLastPoint()
    {
	    return myLastPoint;
    }

    /**
     * returns the PieceType of the piece on the GameBoard at Point point
     * @param point
     * @return PieceType
     */

    public PieceType getPieceOnBoard(Point point)
    {
	    return myBoard[point.x][point.y];
    }

    /**
     * @return PieceType[][] matrix of Board
     */
    public PieceType[][] getBoard() 
    {
	    return myBoard;
    }

    /**
     *
     * Is the board full
     * @return <code>true</code> if the board is full
     *         <code>false</code> if the board isn't full
     */

    public boolean isBoardFull() 
    {
    	for (int i = 0; i < myNumRows; i++) {
    		for (int j = 0; j < myNumColumns; j++) {
    			if (myBoard[j][i] == null) {
    				return false;
    			}
    		}
    	}
    	return true;
    }

    /**
     *
     * Is the column full
     * @return <code>true</code> if the column is full
     *         <code>false</code> if the column isn't full
     */

    public boolean isColumnFull(int col) 
    {
        for (int i = 0; i < myNumRows; i++) {
            if (myBoard[col][i] == null) {
                return false;
            }
        }
	    return true;
    }

    /**
     *
     * Is there a win
     * @return <code>true</code> if game has a win
     *         <code>false</code> if game has a win
     */

    public boolean getIsAWin() 
    {
	    return checkIfWin();
    }

    /**
     * Check if all the spots are null
     * @return <code>true</code> if all spots are null
     *         <code>false</code> if not all spots are null
     */
    
    public boolean checkAllNull()
    {
    	for (int i = 0; i < myNumRows; i++) {
    		for (int j = 0; j < myNumColumns; j++) {
    			if (myBoard[j][i] != null) {
    				return false;
    			}
    		}
    	}
    	return true;
    }

    /**
     * Return number of rows in the GameBoard
     * @return myNumRows
     */
    public int getMyNumRows() {
        return myNumRows;
    }

    /**
     * Return number of rows in the GameBoard
     * @return myNumRows
     */

    public int getMyNumColumns() {
        return myNumColumns;
    }
}