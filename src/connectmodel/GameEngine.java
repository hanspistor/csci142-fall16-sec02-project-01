package connectmodel;

import java.awt.*;
import java.util.Arrays;
import java.util.Vector;

public class GameEngine
{
	private Vector<Player> myPlayers;
	private Player myPlayerUp;
	private Player myStartingPlayer;
	private GameBoard myGameBoard;
  
	public GameEngine(Player player, GameBoard gameBoard)
	{

        myPlayers = new Vector<Player>();
        myPlayers.add(player);
        PieceType computerPieceType = PieceType.BLACK;
        if (player != null && player.getPieceType() == PieceType.BLACK) {
            computerPieceType = PieceType.RED;
        }
        myPlayers.add(new ComputerPlayer("Computer", computerPieceType));
	    myGameBoard = gameBoard;
        selectStartingPlayer(player);
	}

	public boolean selectStartingPlayer(Player player)
	{
        if (!myPlayers.contains(player)) {
            return false;
        }
        myStartingPlayer = player;
        myPlayerUp = player;
		return player == myStartingPlayer;
	}

	public boolean startGame() 
	{
        if (myGameBoard == null || myPlayers.get(0) == null || myPlayers.get(1) == null) {
            return false;
        }
        switchPlayerUp();
        selectStartingPlayer(myPlayerUp);
        myGameBoard.resetBoard();
		return myGameBoard.checkAllNull() == true;
	}
	
	public Player switchPlayerUp() 
	{
        myPlayerUp = myPlayers.get((myPlayers.indexOf(myPlayerUp) + 1) % 2);
        return myPlayerUp;
	}

	public boolean placePiece(int column)
	{
        if (myGameBoard.placePiece(column, myPlayerUp.getPieceType())) {
            myGameBoard.checkIfWin();
            switchPlayerUp();
            return true;
        } else {
            return false;
        }
	}

	public Player getPlayerUp() 
	{
		return myPlayerUp;
	}
	
	public Player getStartingPlayer()
	{
		return myStartingPlayer;
	}

	public Vector<Player> getPlayers()
	{
		return myPlayers;
	}
  
	public void setGameBoard(GameBoard gameboard)
	{
        myGameBoard = gameboard;

	}

	public GameBoard getGameBoard()
	{
		return myGameBoard;
	}
	
}