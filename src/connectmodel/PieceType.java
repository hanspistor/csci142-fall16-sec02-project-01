package connectmodel;


public enum PieceType
{
    RED ("RED"),
    BLACK ("BLACK"),
    GREEN ("GREEN"),
    YELLOW ("YELLOW");

    private String type;

    private PieceType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return this.type;
    }

}