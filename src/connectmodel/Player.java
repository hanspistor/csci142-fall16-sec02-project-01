package connectmodel;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Player
{
	public static final String DEFAULT_NAME = "JohnCena";
	private String myName;
	private int myNumWins;
	private PieceType myPieceType;
    boolean isRegex;
    final static Pattern pattern = Pattern.compile("^[A-Za-z, ]++$");


	public Player(String name, PieceType type)
	{
		myName = name;
        if(!validateName(myName)) {
            myName = "JohnCena";
        }
		myPieceType = type;
		myNumWins = 0;
	  
	}

    private boolean validateName(String name)
    {
        String namePattern = "^[a-zA-Z0-9]+$";
        if (myName.matches(namePattern)) {
            return true;
        }
        return false;

    }

	public void incrementScore() 
	{
        myNumWins++;
	}
	
	public PieceType getPieceType()
	{
		return myPieceType;
	}

	public String getName() 
	{
		return myName;
	}

	public int getNumWins()
	{
		return myNumWins;
	}
	
	public int getScore()
	{
		return myNumWins;
	}
}